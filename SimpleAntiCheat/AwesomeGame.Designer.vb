﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AwesomeGame
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bAction = New System.Windows.Forms.Button()
        Me.lblScore = New System.Windows.Forms.Label()
        Me.lblScoreNormal = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'bAction
        '
        Me.bAction.Location = New System.Drawing.Point(12, 12)
        Me.bAction.Name = "bAction"
        Me.bAction.Size = New System.Drawing.Size(181, 57)
        Me.bAction.TabIndex = 0
        Me.bAction.Text = "Do something awesome to win points"
        Me.bAction.UseVisualStyleBackColor = True
        '
        'lblScore
        '
        Me.lblScore.AutoSize = True
        Me.lblScore.Location = New System.Drawing.Point(92, 86)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(13, 13)
        Me.lblScore.TabIndex = 1
        Me.lblScore.Text = "0"
        '
        'lblScoreNormal
        '
        Me.lblScoreNormal.AutoSize = True
        Me.lblScoreNormal.Location = New System.Drawing.Point(92, 119)
        Me.lblScoreNormal.Name = "lblScoreNormal"
        Me.lblScoreNormal.Size = New System.Drawing.Size(13, 13)
        Me.lblScoreNormal.TabIndex = 1
        Me.lblScoreNormal.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(206, 141)
        Me.Controls.Add(Me.lblScoreNormal)
        Me.Controls.Add(Me.lblScore)
        Me.Controls.Add(Me.bAction)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bAction As System.Windows.Forms.Button
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents lblScoreNormal As System.Windows.Forms.Label

End Class
