﻿Public Class AwesomeGame

    Dim Score As ScoreContainer
    Dim ScoreNormal As Double = 2

    Private Sub bAction_Click(sender As Object, e As EventArgs) Handles bAction.Click
        Score.ChangeValue(CInt(Score.GetValue() * 2))
        ScoreNormal = ScoreNormal * 2
        lblScore.Text = Score.GetValue()
        lblScoreNormal.Text = ScoreNormal
    End Sub

    Private Sub AwesomeGame_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Score = New ScoreContainer(2)
    End Sub
End Class
