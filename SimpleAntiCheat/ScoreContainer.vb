﻿Public Class ScoreContainer
    Private Property Value As Double
    Private Property Key As Long
    Private Property Buffer As Double
    Private Random As Random
    Private MaxRandom = 999999

    Public Sub New(_Value As Double)
        Random = New Random
        Key = Random.Next(1, MaxRandom)
        ChangeValue(_Value)
    End Sub

    Public Sub ChangeValue(_newValue As Double)
        Value = _newValue
        Key = Random.Next(1, MaxRandom)
        Buffer = Value - Key
    End Sub

    Public Function GetValue() As Double
        Return Buffer + Key
    End Function
End Class
